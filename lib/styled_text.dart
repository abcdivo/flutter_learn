import 'package:flutter/material.dart';

class Styled_text extends StatelessWidget {
  const Styled_text({super.key});

  @override
  Widget build(context) {
    return const Text(
      'Text World!',
      style: TextStyle(
        color: Colors.white,
        fontSize: 28,
      ),
    );
  }
}
